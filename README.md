# Démineur #

Ce dépôt a pour but la remise du code du projet semestriel portant sur le jeu du Démineur. Ce projet a été réalisé par le groupe K de première année de DUT informatique à l'IUT de Nantes.

## Versions

Le jeu a été développé en deux temps et deux grandes versions ont été faites :

* une version console qui permet de jouer au jeu du Démineur directement dans un terminal (sans interface graphique)
* une version graphique qui, elle, permet de jouer plus facilement avec une interface et grâce à la souris  

## Lancement du jeu

Pour compiler et exécuter les versions du jeu, vous pouvez utiliser les fichiers 'mkfile'. Pour Linux utiliser la version avec '.sh' et pour Windows la version '.bat'.

Si vous désirez ne pas utiliser ces fichiers vous pouvez compiler avec la commande suivante :
```
javac -encoding "utf-8" -d bin/ -cp bin/ src/*.java src/jeu/*.java -Xlint
```
et exécuter grâce à la commande suivante :
```
java -cp bin/ Demineur
```
Pour toute information supplémentaire, n'hésitez pas à nous contacter.  

**Le groupe K**