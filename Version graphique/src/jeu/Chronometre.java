package jeu;
import java.lang.System.*;

public class Chronometre {

	private long chrono;
	private long tempsEcoule;
	private long tempsSuplementaire;

	/**
	 * Constructeur
	 */
	public Chronometre() {
		this.chrono = 0;
		this.tempsEcoule = 0;
	}

	/**
	 * Lancement du chronomètre
	 */
	public void start() {
		this.chrono = java.lang.System.currentTimeMillis();
	}

	/**
	 * Arrêt du chronomètre, on stocke le temps dans la variable tempsEcoule
	 */
	public void stop() {
		long chrono2 = java.lang.System.currentTimeMillis();
		this.tempsEcoule = (chrono2 - this.chrono) + tempsSuplementaire;
	}

	/**
	* Fonction qui permet d'ajouter 10 secondes au chrono
	*/
	public void aide(){
		tempsSuplementaire += 10000;
	}

	/**
	 * Accesseur permettant de connaître le temps instantané
	 * @return le temps instantané
	 */
	public long getTempsInstantanee() {
		long chrono2 = java.lang.System.currentTimeMillis();

		return (chrono2 - this.chrono) + tempsSuplementaire;
	}

	/**
	 * Accesseur permettant de connaître le temps écoulé
	 * @return le temps écoulé
	 */
	public long getTempsEcoule() {
		return this.tempsEcoule;
	}

	/**
	 * Accesseur de chrono
	 * @return chrono
	 */
	public long getChrono() {
		return this.chrono;
	}

	/**
	 * Méthode qui permet de remettre chrono à zéro
	 */
	public void setChrono() {
		this.chrono = 0;
	}

	/**
	 * Méthode qui permet de remettre le temps écoulé à zéro
	 */
	public void setTempsEcoule() {
		this.tempsEcoule = 0;
	}

}