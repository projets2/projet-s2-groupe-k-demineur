package jeu;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Font;
import java.awt.FontMetrics;

import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class Bouton extends JButton implements MouseListener{

	private Fenetre fenetre;
	private String name;
	private Color couleur1 = new Color(157,59,23);
	private Color couleur2 = new Color(234,120,70);
	private Color bgCouleur = couleur1;
	private Color fgCouleur = couleur2;
	private int id;
	private Border border = new LineBorder(Color.GRAY, 1);

	/**
	 * Constructeur
	 * @param  str le texte sur le bouton
	 * @param  fenetre la fenêtre auquel appartient le bouton
	 * @param  id l'identifiant du bouton
	 */
	public Bouton(String str, Fenetre fenetre, int id){
		super(str);
		this.name = str;
		this.addMouseListener(this);
		this.fenetre = fenetre;
		this.id = id;
		this.setBorder(border);
	}

	public void paintComponent(Graphics g){
		// Fond
		g.setColor(this.bgCouleur);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

		// Text
		Font police = new Font("Arial", Font.PLAIN, 20);
		if (this.name == "x" || this.name == "D" || this.name == "?") {
			police = new Font("Arial", Font.BOLD, 30);
		}
		FontMetrics f = g.getFontMetrics(police);
		g.setFont(police);
		g.setColor(fgCouleur);
		g.drawString(this.name, (this.getWidth() - this.name.length()*(f.getHeight()/2))/2, (this.getHeight() + f.getHeight()/2)/2);
		//g.drawString(this.name, this.getWidth()/2 - this.name.length()*(f.getHeight()/2)/2, this.getHeight()/2 + f.getHeight()/4);
	}

	/**
	 * Méthode appelée lors du clique de la souris
	 */
	public void mouseClicked(MouseEvent event) {
		// Tous les événements sont retournés vers la fenêtre via la méthode evenement en prenant en paramètre le MouseEvent et l'id de la case
		fenetre.evenement(event, id);
	}

	/**
	 * Méthode appelée lors du survol de la souris
	 */
	public void mouseEntered(MouseEvent event) {}

	/**
	 * Méthode appelée lorsque la souris sort de la zone du bouton
	 */
	public void mouseExited(MouseEvent event) {}

	/**
	 * Méthode appelée lorsque l'on presse le bouton gauche de la souris
	 */
	public void mousePressed(MouseEvent event) {}

	/**
	 * Méthode appelée lorsque l'on relâche le clic de souris
	 */
	public void mouseReleased(MouseEvent event) {}

	/**
	 * Méthode qui permet de modifier le texte du Bouton
	 * @param str la chaine de caractère à mettre sur le bouton
	 */
	public void setText(String str){
		this.name = str;
	}

	/**
	 * Méthode qui permet de modifier la couleur du bouton en fonction de si elle est découverte ou non
	 * @param decouverte si true on change la couleur
	 */
	public void setColor(boolean decouverte){
		if (decouverte) {
			bgCouleur = couleur2;
			fgCouleur = couleur1;
		}
	}
}