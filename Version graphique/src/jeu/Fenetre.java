package jeu;

import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.lang.NullPointerException;
import java.text.*;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class Fenetre extends JFrame implements ActionListener {

	// Déclaration des attributs généraux pour la partie
	private Partie partie;
	private int largeur;
	private int hauteur;
	private int nbMines;
	private int nbTours = 0;
	private Joueur joueur;

	// Attribut pour les différents boutons de la grille de jeu
	private Bouton b[];

	// Déclaration des différentes panel et autres attributs globaux
	CardLayout mainLayout = new CardLayout();
	JPanel parentComponent = new JPanel(mainLayout);
	JPanel jeuPanelContainer;
	JPanel mainMenuPanel, mainMenuScore, mainMenuAide, mainMenuPropos;
	JPanel mainMenuPerso, mainMenuPersoCentre;
	JPanel infoPanel;
	JFormattedTextField tailleX, tailleY, nombMine;
	JLabel message = new JLabel("");
	JLabel tempsLabel, drapeauxLabel;
	Thread threadTimer;

	// Déclarations des attributs de la barre de menu
	JMenuBar menuBar = new JMenuBar();
	JMenu menuFichier = new JMenu("Fichier");
	JMenu menuScore = new JMenu("Score");
	JMenu menuAide = new JMenu("Aide");
	JMenuItem menuFichier_items0 = new JMenuItem("Nouvelle Partie");
	JMenuItem menuFichier_items1 = new JMenuItem("Options");
	JMenuItem menuFichier_items2 = new JMenuItem("Quitter");
	JMenuItem menuScore_items0 = new JMenuItem("Classement");
	JMenuItem menuAide_items0 = new JMenuItem("Aide");
	JMenuItem menuAide_items1 = new JMenuItem("A propos");

	// Déclaration des différents panneaux (noms)
	private final String PANEL1 = "jeu";
	private final String PANEL2 = "menu";
	private final String PANEL3 = "perso";
	private final String PANEL4 = "score";
	private final String PANEL5 = "aide";
	private final String PANEL6 = "propos";

	/**
	 * Constructeur
	 */
	public Fenetre(){

		// Création de la barre de menu
		menuFichier_items0.addActionListener(this);
		menuFichier_items0.setActionCommand("recommencer");
		menuFichier.add(menuFichier_items0);
		menuFichier_items1.addActionListener(this);
		menuFichier_items1.setActionCommand("personnalisation");
		menuFichier.add(menuFichier_items1);
		menuFichier_items2.addActionListener(this);
		menuFichier_items2.setActionCommand("quit");
		menuFichier.add(menuFichier_items2);
		menuScore_items0.addActionListener(this);
		menuScore_items0.setActionCommand("classement");
		menuScore.add(menuScore_items0);
		menuAide_items0.addActionListener(this);
		menuAide_items0.setActionCommand("aide");
		menuAide.add(menuAide_items0);
		menuAide_items1.addActionListener(this);
		menuAide_items1.setActionCommand("propos");
		menuAide.add(menuAide_items1);

		menuBar.add(menuFichier);
		menuBar.add(menuScore);
		menuBar.add(menuAide);
		this.setJMenuBar(menuBar);

		// On créer un joueur
		this.joueur = new Joueur();

		// Titre de la fenêtre
		this.setTitle("Démineur");

		// Taille de la fenêtre
		this.setSize(320, 320);

		// Je sais pas à quoi ça sert
		this.setLocationRelativeTo(null);

		// Pour fermer la fenêtre quand on quitte
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// On créé le panneau du Menu
		panneauMenu();
		// On créé le panneau de Jeu
		panneauJeu();
		// On créé le panneau de personnalisation de la grille
		panneauPersonnalisation();
		// On créé le panneau des scores (classement)
		panneauScore();
		// On créé le panneau aide et à propos
		panneauAide();
		panneauPropos();

		// On définie le layout du JFrame
		this.setLayout(this.mainLayout);

		// On ajoute les différents panels au JPanel du CardLayout
		this.parentComponent.add(this.mainMenuPanel, PANEL2);
		this.parentComponent.add(this.mainMenuScore, PANEL4);
		this.parentComponent.add(this.mainMenuPerso, PANEL3);
		this.parentComponent.add(this.jeuPanelContainer, PANEL1);
		this.parentComponent.add(this.mainMenuAide, PANEL5);
		this.parentComponent.add(this.mainMenuPropos, PANEL6);

		// On ajoute pour finir a la JFrame le conteneur de toute l'app
		this.getContentPane().add(this.parentComponent);

		// On ajoute l’icône de l'application
		setIconImage(new ImageIcon(this.getClass().getResource("bomb.png")).getImage());

		// On change l'apparence de la fenêtre Java avec comme nouveau style Nimbus
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			SwingUtilities.updateComponentTreeUI(this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// On rend la fenêtre visible
		this.setVisible(true);
	}

	/**
	 * Méthode qui permet de créer le panneau du menu principal
	 */
	public void panneauMenu(){

		// On créé le JPanel avec un GridLayout de 2 sur 2
		mainMenuPanel = new JPanel(new GridLayout(2,2));

		// On créé les 4 boutons de choix et on leur ajoute une ActionCommand qui sera traitée dans le actionPerformed

		JButton choix1 = new JButton("16x16 - 20 mines");
		mainMenuPanel.add(choix1);
		choix1.setActionCommand("choix1");
		choix1.addActionListener(this);

		JButton choix2 = new JButton("30x30 - 100 mines");
		mainMenuPanel.add(choix2);
		choix2.setActionCommand("choix2");
		choix2.addActionListener(this);

		JButton choix3 = new JButton("50x50 - 500 mines");
		mainMenuPanel.add(choix3);
		choix3.setActionCommand("choix3");
		choix3.addActionListener(this);

		JButton personnalisation = new JButton("Personnalisation");
		mainMenuPanel.add(personnalisation);
		personnalisation.setActionCommand("personnalisation");
		personnalisation.addActionListener(this);
	}

	/**
	 * Méthode qui permet de créer le panneau conteneur général du jeu
	 */
	public void panneauJeu(){
		// On créer et on défini pour le panel contenant le jeu un BorderLayout
		jeuPanelContainer = new JPanel(new BorderLayout());
	}

	/**
	 * Méthode qui permet de gérer les différents événements principaux du programme
	 * @param arg0 l'ActionEvent associé
	 */
	public void actionPerformed(ActionEvent arg0) {
		// On récupère la valeur du choix (qu'on avait attribué au bouton quand on a fait un setActionCommand)
		String choix = arg0.getActionCommand();

		// On affiche tel ou tel panneau en fonction de la valeur du choix
		if (choix == "choix1") {
			creationGrille(16,16,20);
			mainLayout.show(parentComponent,PANEL1);
		}else if (choix == "choix2") {
			creationGrille(30,30,100);
			mainLayout.show(parentComponent,PANEL1);
		}else if (choix == "choix3") {
			creationGrille(50,50,500);
			mainLayout.show(parentComponent,PANEL4);
		}else if (choix == "personnalisation") {
			mainLayout.show(parentComponent,PANEL3);
		}else if (choix == "recommencer") {
			Fenetre f = new Fenetre();
			this.dispose();
		}else if(choix == "classement") {
			mainLayout.show(parentComponent, PANEL4);
		}else if(choix == "retourAuJeu") {
			// On teste si une partie est en cours et si oui on retourne vers cette partie
			if (partie != null && !partie.estTerminee()) {
				mainLayout.show(parentComponent, PANEL1);
			}else {	// Sinon on retourne vers le panneau principal
				Fenetre f = new Fenetre();
				this.dispose();
			}
		}else if (choix == "aide") {
			mainLayout.show(parentComponent, PANEL5);
		}else if (choix == "propos") {
			mainLayout.show(parentComponent, PANEL6);
		}else if (choix == "quit") {
			System.exit(0);
		}
	}

	/**
	 * Méthode qui permet de créer le panneau de personnalisation d'une partie
	 */
	private void panneauPersonnalisation(){
		mainMenuPerso = new JPanel();
		mainMenuPerso.setLayout(new BorderLayout());
		mainMenuPerso.setBorder(new EmptyBorder(10, 10, 10, 10));

		mainMenuPersoCentre = new JPanel();
		mainMenuPersoCentre.setLayout(new GridLayout(5, 1, 10, 10));

		JPanel panneauPerso = new JPanel();
		panneauPerso.setLayout(new FlowLayout(FlowLayout.CENTER));

		JLabel titrePerso = new JLabel("Personnalisation");
		Font font = titrePerso.getFont();
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		titrePerso.setFont(boldFont);
		titrePerso.setHorizontalAlignment(JLabel.CENTER);
		titrePerso.setBorder(new EmptyBorder(0, 0, 10, 0));

		Font police = new Font("Tahoma", Font.BOLD, 16);
		message.setFont(police);
		message.setForeground(Color.RED);
		message.setHorizontalAlignment(JLabel.CENTER);

		JLabel l1 = new JLabel("Hauteur");
		JLabel l2 = new JLabel("Largeur");
		JLabel l3 = new JLabel("Nombre de mines");
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		p1.setLayout(new GridLayout(1, 2, 5, 0));
		p2.setLayout(new GridLayout(1, 2, 5, 0));
		p3.setLayout(new GridLayout(1, 2, 5, 0));
		tailleY = new JFormattedTextField(NumberFormat.getIntegerInstance());
		tailleX = new JFormattedTextField(NumberFormat.getIntegerInstance());
		nombMine = new JFormattedTextField(NumberFormat.getIntegerInstance());
		p1.add(l1);
		p1.add(tailleY);
		p2.add(l2);
		p2.add(tailleX);
		p3.add(l3);
		p3.add(nombMine);

		JButton confirm = new JButton("Confirmer");
		JButton annule = new JButton("Annuler");

		JPanel confirmAnnul = new JPanel();
		confirmAnnul.setLayout(new GridLayout(1, 2, 5, 0));

		confirmAnnul.add(annule);
		confirmAnnul.add(confirm);

		mainMenuPersoCentre.add(p1);
		mainMenuPersoCentre.add(p2);
		mainMenuPersoCentre.add(p3);
		mainMenuPersoCentre.add(confirmAnnul);
		mainMenuPersoCentre.add(message);

		panneauPerso.add(mainMenuPersoCentre);

		mainMenuPerso.add(titrePerso, BorderLayout.NORTH);
		mainMenuPerso.add(panneauPerso, BorderLayout.CENTER);

		annule.addActionListener(new AnnulPersoListener());
		confirm.addActionListener(new ConfirmPersoListener());
	}

	/**
	 * Méthode qui permet de créer le panneau des scores
	 */
	private void panneauScore(){
		mainMenuScore = new JPanel(new BorderLayout());
		mainMenuScore.setBorder(new EmptyBorder(10, 10, 10, 10));

		// On créé le titre du panneau
		JLabel titre = new JLabel("Classement");
		Font font = titre.getFont();
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		titre.setFont(boldFont);
		titre.setHorizontalAlignment(JLabel.CENTER);
		titre.setBorder(new EmptyBorder(0, 0, 10, 0));

		// On créé le panneau de classement
		JPanel panelClassement = new JPanel(new BorderLayout());

		// On récupère les données de classement sous forme d'un tableau d'Object (de String ici)
		Object[][] donnees = Classement.toTable();
		String[] entetes = {"Rang", "Pseudo", "Score", "Temps"};

		// On créé le tableau contenant le classement
		JTable tableau = new JTable(donnees, entetes);
		tableau.setSelectionBackground(Color.GRAY);
		tableau.setSelectionForeground(Color.WHITE);
		tableau.setGridColor(Color.LIGHT_GRAY);

		// On ajoute les différents événements au panelClassement
		panelClassement.add(tableau.getTableHeader(), BorderLayout.NORTH);
		panelClassement.add(tableau, BorderLayout.CENTER);

		// On ajoute le titre et le panelClassement au panneau du score
		mainMenuScore.add(titre, BorderLayout.NORTH);
		mainMenuScore.add(panelClassement, BorderLayout.CENTER);
	}

	class ConfirmPersoListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			int tailleHauteur, tailleLargeur, mines;

			tailleLargeur = Integer.parseInt(tailleX.getText());
			tailleHauteur = Integer.parseInt(tailleY.getText());
			mines = Integer.parseInt(nombMine.getText());
			if((tailleLargeur > 3 && tailleLargeur < 30) && (tailleHauteur > 3 && tailleHauteur < 30)){
				if(mines > 0 && mines < ((tailleHauteur*tailleLargeur)-10)){
					creationGrille(tailleLargeur,tailleHauteur,mines);
					mainLayout.show(parentComponent,PANEL1);
				}else{
					message.setText("Erreur : nombre de mine");
				}
			}else{
				message.setText("Erreur : taille");
			}
		}
	}

	class AnnulPersoListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Fenetre f = new Fenetre();
			dispose();
		}
	}

	/**
	 * Méthode qui permet de créer la grille de jeu
	 */
	public void creationGrille(int haut, int larg, int nbM){
		GridLayout grilleJeuLayoutContainer;
		JPanel grilleJeuPanelContainer;

		this.hauteur = haut;
		this.largeur = larg;
		this.nbMines = nbM;

		this.setSize(this.largeur * 50, this.largeur * 50);
		this.setLocationRelativeTo(null);

		// On créé la partie
		partie = new Partie(this.joueur, this.largeur, this.hauteur, this.nbMines);

		// On initialise le tableau de boutons
		b = new Bouton[this.largeur * this.hauteur];
		for (int i = 0; i < this.largeur * this.hauteur; i++) {
				b[i] = new Bouton("", this, i);
		}

		grilleJeuLayoutContainer = new GridLayout(this.largeur, this.hauteur);
		grilleJeuPanelContainer = new JPanel(grilleJeuLayoutContainer);

		// On ajoute une ActionCommand (leur indice) aux différents boutons de la grille de jeu
		for (int i = 0; i < this.largeur * this.hauteur; i++) {
			grilleJeuPanelContainer.add(b[i]);
			b[i].setActionCommand(""+i);
			b[i].addActionListener(this);
		}

		// On créé le panneau d'information de la partie (qui contient le temps et le nombre de drapeaux en cours)
		JPanel infoBar = new JPanel();
		tempsLabel = new JLabel(" Durée : 0");
		drapeauxLabel = new JLabel("Drapeaux : 0/"+this.nbMines+" ");
		infoBar.add(drapeauxLabel);
		infoBar.add(tempsLabel);

		// Création d'un thread pour le chronomètre
		threadTimer = new Thread(new Runnable() {
			public void run() {
				while (!partie.estTerminee()) {
					try {
						tempsLabel.setText(" Durée : " + partie.getTemps());
						Thread.sleep(1000);
					} catch (InterruptedException ex) {
						ex.printStackTrace();
					}
				}
			}
		});

		// On ajoute les différents éléments au container général (jeuPanelContainer)
		jeuPanelContainer.add(infoBar, BorderLayout.NORTH);
		jeuPanelContainer.add(grilleJeuPanelContainer, BorderLayout.CENTER);

		this.revalidate();
	}

	/**
	 * Méthode qui gère les différents événements souris
	 * @param event le MouseEvent associé au clique du bouton
	 * @param indice l'indice de la case cliqué
	 */
	public void evenement(MouseEvent event, int indice){
		boolean partieTerminee = partie.estTerminee();
		boolean partieGagnee = partie.aGagne();

		if (!(partieTerminee || partieGagnee)) {
			if (event.getButton() == MouseEvent.BUTTON1) {
				// Clic gauche
				Coordonnee coord = new Coordonnee(indice % this.largeur,indice / this.largeur);
				// Si c'est le premier tour
				if (nbTours == 0) {
					// Initialise les bombes et lance le chrono
					partie.initialiserBombe(coord);
					// On démarre le thread de rafraîchissement du chrono
					threadTimer.start();
					nbTours++;
					sync();
				}
				partie.initCoordControle();
				partie.decouvrir(coord);
			}else if(event.getButton() == MouseEvent.BUTTON3){
				// Clic droit
				partie.marquage(new Coordonnee(indice % this.largeur,indice / this.largeur));
			}
			sync();
		}

		partieGagnee = partie.aGagne();
		partieTerminee = partie.estTerminee();

		// On vérifie si la partie est terminée
		if (partieTerminee) {
			// Si elle est terminée on arrête le thread du chrono
			// threadTimer.cancel();
			if (partieGagnee) {
				// On demande au joueur s'il veut recommencer
				Object[] optionsbis = { "Bien sûr", "Non" };
				int choice = JOptionPane.showOptionDialog(null, "Vous avez gagné !\nVoulez-vous démarrer une nouvelle partie?", "Gagné !", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionsbis, optionsbis[0]);

				// On demande le pseudo du joueur et on l'ajoute au classement
				String pseudo = "";
				do {
					pseudo = JOptionPane.showInputDialog("Ton pseudo");
				}while (pseudo.length() == 0 || pseudo.length() > 15);
				joueur.setPseudo(pseudo);
				Classement.add(joueur);

				// Si le joueur choisi de recommencer
				if(choice == 0){
					Fenetre f = new Fenetre();
					this.dispose();
				}else{
					this.dispose();
				}
			}else{
				// On demande au joueur s'il veut recommencer
				Object[] options = { "Bien sûr", "Non" };
				int choice = JOptionPane.showOptionDialog(null, "Vous avez perdu !\nVoulez-vous démarrer une nouvelle partie?", "Perdu !", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);

				if(choice == 0){
					Fenetre f = new Fenetre();
					this.dispose();
				}else{
					this.dispose();
				}
			}
		}
	}

	/**
	 * Méthode qui synchronise la fenêtre avec la partie : qui modifie le texte, la couleur des différents boutons en fonction de leur état et de leur type
	 */
	public void sync() {
		Case[][] grille = partie.getGrille();

		// On parcours la grille de jeu
		for(int j = 0; j < this.hauteur; j++){
			for (int i = 0; i < this.largeur; i++) {
				// Si la case est découverte
				if (grille[i][j].getDecouverte() == true) {
					// et que celle-ci est numérotée
					if (grille[i][j] instanceof CaseNumerotee) {
						CaseNumerotee caseN = (CaseNumerotee)grille[i][j];
						// si la case n'a pas de voisine (numérotée à 0)
						if (caseN.getNbCasesVoisinesMinees() == 0) {
							// On affiche aucun texte dans celle-ci
							b[(j * this.largeur) + i].setText("");
						}else{
							// Sinon on affiche son nombre de cases voisines
							b[(j * this.largeur) + i].setText("" + grille[i][j]);
						}
					// sinon elle est minée
					}else {
						// On affiche une croix dans celle-ci
						b[(j * this.largeur) + i].setText("x");
					}
					// on change la couleur de la case (car découverte)
					b[(j * this.largeur) + i].setColor(true);
				}else{
					// Sinon on affiche rien dans celle-ci
					b[(j * this.largeur) + i].setText("");
				}

				// Si la case est marquée par un drapeau
				if (grille[i][j].getMarquage() == Marquage.Drapeau) {
					// On affiche "D" dans celle-ci
					b[(j * this.largeur) + i].setText("D");
					// On modifie le nombre de drapeau dans la barre d'information de la partie
					drapeauxLabel.setText("Drapeaux : "+Case.getNbDrapeau()+"/"+this.nbMines);
				// Si la case est marquée d'un point d'interrogation
				}else if(grille[i][j].getMarquage() == Marquage.Interrogation) {
					// On affiche "?" dans celle-ci
					b[(j * this.largeur) + i].setText("?");
					// On modifie le nombre de drapeau dans la barre d'information de la partie
					drapeauxLabel.setText("Drapeaux : "+Case.getNbDrapeau()+"/"+this.nbMines);
				}

				// On rafraîchit le bouton
				b[(j * this.largeur) + i].repaint();
			}
		}
	}

	/**
	 * Méthode qui permet de créer le panneau d'aide
	 */
	public void panneauAide() {
		mainMenuAide = new JPanel(new BorderLayout());setIconImage(Toolkit.getDefaultToolkit().getImage("C:/myDirectory/myIcon.jpg"));
		mainMenuAide.setBorder(new EmptyBorder(10, 5, 5, 5));

		// On créé le titre du panneau
		JLabel titre = new JLabel("Aide");
		Font font = titre.getFont();
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		titre.setFont(boldFont);
		titre.setHorizontalAlignment(JLabel.CENTER);
		titre.setBorder(new EmptyBorder(0, 0, 10, 0));

		// On créé le texte (JTextArea) et on le paramètre
		JTextArea texte = new JTextArea("L’objectif du jeu est de découvrir le plus rapidement possible toutes les mines cachées en les évitant (ne pas cliquer dessus sinon vous perdez la partie). Le plateau de jeu est rectangulaire contenant de petites cases carrées (toutes recouvertes au départ). Le plateau est de taille variable (défini au départ ou paramétrable) avec un nombre défini (ou paramétrable) de mines. Votre but est donc de découvrir l’ensemble des cases non minées dans un temps minimum. Vous disposez, par ailleurs, “d’outils” permettant de vous aider (drapeau, point d’interrogation).\n\r\n\rPour marquer une case faites un clique droit sur la case ciblée. Répétez pour changer de valeur : drapeau, point d'interrogation ou enlever.\n\r\n\rLorsque vous gagnez une partie vous pouvez entrer votre pseudo. Vous pourrez par la suite (via l'onglet \"Score\") consulter le classement des différents joueurs.");
		texte.setBackground(null);
		texte.setEditable(false);
		texte.setLineWrap(true);
		texte.setWrapStyleWord(true);
		texte.setFocusable(false);

		// On ajoute une barre de défilement
		JScrollPane scroll = new JScrollPane(texte);

		// On ajoute un bouton retour (en ajoutant un événement "retourAuJeu")
		JButton retour = new JButton("Retour");
		retour.setActionCommand("retourAuJeu");
		retour.addActionListener(this);

		// On ajoute les différents éléments au panneau d'aide
		mainMenuAide.add(titre, BorderLayout.NORTH);
		mainMenuAide.add(scroll, BorderLayout.CENTER);
		mainMenuAide.add(retour, BorderLayout.SOUTH);
	}

	/**
	 * Méthode qui permet de créer le panneau à propos
	 */
	public void panneauPropos() {
		mainMenuPropos = new JPanel(new BorderLayout());
		mainMenuPropos.setBorder(new EmptyBorder(10, 5, 5, 5));

		JLabel titre = new JLabel("Jeu du démineur");
		Font font = titre.getFont();
		Font font2 = new Font(font.getFontName(), Font.BOLD, 18);
		titre.setFont(font2);
		titre.setHorizontalAlignment(JLabel.CENTER);
		titre.setBorder(new EmptyBorder(0, 0, 10, 0));



		JLabel texte = new JLabel("Projet de semestre 2 - Groupe K - 2014");
		texte.setHorizontalAlignment(JLabel.CENTER);
		texte.setBorder(new EmptyBorder(10, 0, 5, 0));

		mainMenuPropos.add(titre, BorderLayout.NORTH);
		mainMenuPropos.add(texte, BorderLayout.CENTER);
	}
}