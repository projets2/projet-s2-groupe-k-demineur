package jeu;

public class ListeCoord {
    private Coordonnee[] c;
    private int indice;
    private int taille;

    public ListeCoord(int taille){
        c = new Coordonnee[taille];
        indice = 0;
        this.taille = taille;
    }

    public void add(Coordonnee coord){
       c[indice] =  coord;
       indice++;
    }

    public boolean contains(Coordonnee coord){
        boolean in = false;
        for (int i = 0; i < indice; i++) {
            if (c[i] != null) {
                if (c[i].equals(coord)) {
                    in = true;
                }
            }else{
                System.out.println("null");
            }
        }
        return in;
    }

    public void afficher(){
        for (int i = 0; i < indice; i++) {
            if (c[i] != null) {
                System.out.println(c[i]);
            }else {
                System.out.println("null");
            }
        }
    }

}