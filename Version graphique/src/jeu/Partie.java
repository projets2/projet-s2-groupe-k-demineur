package jeu;

import java.util.*;
import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.NullPointerException;

public class Partie {

	private int nbMines;
	private int largeur;
	private int hauteur;
	private boolean etat;
	private boolean gagner;
	private Joueur joueur;
	private Case[][] grille;
	private Chronometre temps;
	// Liste regroupant l'ensemble des coordonnees des cases minees
	private ArrayList<Coordonnee> caseMinee;
	// Liste regroupant les coordonnees déjà controlées
	private ListeCoord coordControle;

	/**
	 * Constructeur
	 * @param  joueur  le pseudo du joueur
	 * @param  hauteur la hauteur de la grille
	 * @param  largeur la largeur de la grille
	 * @param  nbMines la nombre de mine(s) de la grille
	 */
	public Partie(Joueur joueur, int hauteur, int largeur, int nbMines){
		this.joueur = joueur;
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.nbMines = nbMines;
		this.grille = new Case[this.largeur][this.hauteur];
		this.caseMinee = new ArrayList<Coordonnee>();
		this.etat = true;
		this.gagner = false;
		this.temps = new Chronometre();
		coordControle = new ListeCoord(hauteur * largeur);
		initialiserGrille();
		CaseNumerotee.init();
		Case.init();
	}

	/**
	 * Méthode qui permet d'initialiser la grille de jeu
	 */
	public void initialiserGrille(){
		for(int j = 0; j < this.hauteur; j++){
			for (int i = 0; i < this.largeur; i++) {
				this.grille[i][j] = new CaseNumerotee();
			}
		}
	}

	/**
	 * Méthode qui initialise le placement des bombes et démarre le chronomètre
	 * @param coord la première coordonnée entrée par l'utilisateur qui ne doit pas être minée
	 */
	public void initialiserBombe(Coordonnee coord){
		int coorX = coord.getX();
		int coorY = coord.getY();
		boolean coordonneeDejaPris = false;
		int nombreMine = 0;
		Random rand;
		int x;
		int y;

		// Tant que le nombre de mines placées et différent du nombre de mines à placer
		while(nombreMine != this.nbMines){
			// On tire des coordonnées aléatoires
			rand = new Random();
			x = rand.nextInt(this.largeur);
			y = rand.nextInt(this.hauteur);

			// On vérifie que cette coordonnées n'a pas déjà une mines
			coordonneeDejaPris = false;
			for(int i = 0; i < caseMinee.size(); i++) {
				Coordonnee c = new Coordonnee(x,y);
				if(caseMinee.get(i).equals(c)){
					coordonneeDejaPris = true;
					break;
				}
			}

			// Si la coordonnée n'est pas prise et est différente de la case de départ on rend la case minée
			if(!coordonneeDejaPris && (x != coorX || y != coorY)){
				this.grille[x][y] = new CaseMinee();
				caseMinee.add(new Coordonnee(x,y));
				nombreMine++;
			}
		}

		// Pour chaque case minée on calcule le nombre de cases minées voisines
		for (int i = 0; i < caseMinee.size(); i++) {
			Coordonnee c = caseMinee.get(i);
			x = c.getX();
			y = c.getY();

			if (((x+1) < largeur) && (grille[x+1][y] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x+1][y]).addCasesVoisinesMinees();
			}
			if (((y+1) < hauteur) && (grille[x][y+1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x][y+1]).addCasesVoisinesMinees();
			}
			if (((x-1) >= 0) && (grille[x-1][y] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x-1][y]).addCasesVoisinesMinees();
			}
			if (((y-1) >= 0) && (grille[x][y-1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x][y-1]).addCasesVoisinesMinees();
			}
			if (((x+1) < largeur) && ((y+1) < hauteur) && (grille[x+1][y+1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x+1][y+1]).addCasesVoisinesMinees();
			}
			if (((x+1) < largeur) && ((y-1) >= 0) && (grille[x+1][y-1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x+1][y-1]).addCasesVoisinesMinees();
			}
			if (((x-1) >= 0) && ((y+1) < hauteur) && (grille[x-1][y+1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x-1][y+1]).addCasesVoisinesMinees();
			}
			if (((x-1) >= 0) && ((y-1) >= 0) && (grille[x-1][y-1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x-1][y-1]).addCasesVoisinesMinees();
			}
		}

		// On lance le chronomètre
		this.temps.start();

	}

	/**
	 * Méthode qui permet de découvrir les cases en fonction des cases voisines et des cases minées
	 * @param coord la coordonnée de départ de la découverte
	 */
	public void decouvrir(Coordonnee coord){

		int x = coord.getX();
		int y = coord.getY();
		Case c = grille[x][y];
		coordControle.add(coord);

		// On teste si une case est non marqué pour pouvoir la découvrir
		if (c.getMarquage() == Marquage.NonMarque) {
			// Si c'est une case minee
			if (c instanceof CaseMinee) {
				// la partie est terminée
				terminer();
			}else {
				// Sinon la case est une case numerotee
				CaseNumerotee cNum = (CaseNumerotee)c;
				// on la decouvre
				cNum.decouvrir();
				if (cNum.getNbCasesVoisinesMinees() == 0) {
					// Création d'une variable Coordonnee temporaire
					Coordonnee tmp = new Coordonnee(x+1,y);
					// Si la coordonnée existe (pas hors des limites de la grille) et qu'elle n'a pas été déjà controlée
					if ((x+1) < largeur && ! coordControle.contains(tmp)) {
						// Alors on rappele la méthode avec cette coordonnée
						decouvrir(tmp);
					}
					tmp = new Coordonnee(x,y+1);
					if ((y+1) < hauteur && ! coordControle.contains(tmp)) {
						decouvrir(tmp);
					}
					tmp = new Coordonnee(x-1,y);
					if ((x-1) >= 0 && ! coordControle.contains(tmp)) {
						decouvrir(tmp);
					}
					tmp = new Coordonnee(x,y-1);
					if ((y-1) >= 0 && ! coordControle.contains(tmp)) {
						decouvrir(tmp);
					}
					tmp = new Coordonnee(x+1,y-1);
					if (((x+1) < largeur) && ((y-1) >= 0) && ! coordControle.contains(tmp)) {
						decouvrir(tmp);
					}
					tmp = new Coordonnee(x-1,y+1);
					if (((x-1) >= 0) && ((y+1) < hauteur) && ! coordControle.contains(tmp)) {
						decouvrir(tmp);
					}
					tmp = new Coordonnee(x-1,y-1);
					if ((((x-1) >= 0) && ((y-1) >= 0)) && ! coordControle.contains(tmp)) {
						decouvrir(tmp);
					}
					tmp = new Coordonnee(x+1,y+1);
					if (((x+1) < largeur) && ((y+1) < hauteur) && ! coordControle.contains(tmp)) {
						decouvrir(tmp);
					}
				}
			}
		}

		if (!estTerminee()) {
			joueur.setTemps(this.temps.getTempsInstantanee());
		}
	}

	/**
	 * Méthode permettant d'initialiser la liste des coordonnées contrôlées
	 */
	public void initCoordControle(){
		coordControle = new ListeCoord(this.hauteur * this.largeur);
	}

	/**
	 * Méthode qui permet de marquer une case
	 * @param coord les coordonnées de la case à miner
	 */
	public void marquage(Coordonnee coord) {
		if ((coord.getX() >= 0) && (coord.getX() < this.largeur) && (coord.getY() >= 0) && (coord.getY() < this.hauteur)) {
			if (grille[coord.getX()][coord.getY()].getDecouverte() == false) {
				grille[coord.getX()][coord.getY()].marquer();
			}else{
				System.out.println("La case que vous essayez de marquer est découverte.");
			}
		}else {
			System.out.println("La case que vous essayez de marquer n'existe pas.");
		}
	}

	/**
	 * Methode qui permet de terminer la partie
	 */
	public void terminer(){
		etat = false;
		afficherMine();

		// On calcule de temps que le joueur a mit pour finir la partie
		this.temps.stop();
		joueur.setTemps(this.temps.getTempsEcoule() / 1000);

		// On calcule le score du joueur au final
		int tmpScore = 0;
		if (this.gagner) {
			if (joueur.getTemps() != 0) {
				int temps = (int)joueur.getTemps();
				temps = temps == 0 ? 1 : temps;
				System.out.println(temps);
				tmpScore = (largeur*hauteur+(nbMines*10))/(temps);
			}
		}
		joueur.setScore(tmpScore);
	}

	/**
	 * Methode qui permet de savoir si une partie est terminee ou non
	 * @return true si la partie est terminee, false sinon
	 */
	public boolean estTerminee() {
		return !etat;
	}

	/**
	 * Méthode qui permet de découvrir toutes les cases minées
	 */
	public void afficherMine(){
		Coordonnee coordMine;
		for (int i = 0; i < caseMinee.size(); i++) {
				coordMine = caseMinee.get(i);
				grille[coordMine.getX()][coordMine.getY()].decouvrir();
			}
	}

	/**
	 * Méthode qui permet de savoir si la partie a été gagnée ou non.
	 * Postcondition: si la partie est gagnée la méthode terminer() est appelée
	 * @return true si la partie a été gagnée, false sinon
	 */
	public boolean aGagne(){
		int tmp = CaseNumerotee.getNbDeFoisDecouverte();
		if(tmp == (this.hauteur*this.largeur)-this.nbMines){
			this.gagner = true;
			terminer();
		}
		return this.gagner;
	}

	/**
	 * Méthode que permet de connaître le temps écoulé depuis le début de la partie
	 * @return le temps écoulé depuis le commencement de la partie
	 */
	public String getTemps(){
		long tmpTemps = this.temps.getTempsInstantanee() / 1000;
		long minute = tmpTemps / 60;
		long seconde = tmpTemps % 60;

		return minute + "min " + seconde + "s";
	}

	/**
	* Fonction qui découvre une case (non minée) au hasard et qui ajoute 10 secondes au chrono
	*/
	public void aide(){
		// Découverte de la case
		Case c;
		Random r = new Random();
		do{
			int i = r.nextInt(this.largeur);
			int j = r.nextInt(this.hauteur);
			c = this.grille[i][j];
		}while(c instanceof CaseMinee);
		if (c instanceof CaseNumerotee) {
			c.decouvrir();
		}
		else{
			System.out.println("Erreur !");
		}

		// Ajout du temps au chrono
		temps.aide();
	}

	/**
	 * Accesseur qui permet de récupérer la largeur
	 * @return la largeur
	 */
	public int getLargeur() {
		return this.largeur;
	}
	/**
	 * Accesseur qui permet de récupérer la hauteur
	 * @return la hauteur
	 */
	public int getHauteur() {
		return this.hauteur;
	}
	/**
	 * Accesseur qui permet de récupérer le nombre de mines
	 * @return le nombre de mines
	 */
	public int getNbMines() {
		return this.nbMines;
	}
	/**
	 * Accesseur qui permet de récupérer le joueur
	 * @return le joueur
	 */
	public Joueur getJoueur() {
		return this.joueur;
	}
	/**
	 * Accesseur qui permet de récupérer le chronomètre
	 * @return le chronomètre
	 */
	public Chronometre getChronometre() {
		return this.temps;
	}
	/**
	 * Accesseur qui permet de récupérer la liste des coordonnées des cases minées
	 * @return la liste des coordonnées des cases minées
	 */
	public ArrayList<Coordonnee> getCaseMinee() {
		return this.caseMinee;
	}
	/**
	 * Accesseur qui permet de récupérer la liste des coordonnées déjà contrôlées
	 * @return la liste des coordonnées déjà contrôlées
	 */
	public ListeCoord getCoordControle() {
		return this.coordControle;
	}
	/**
	 * Accesseur qui permet de récupérer une case de la grille
	 * @param  i la coordonnée horizontale de la case à récupérer
	 * @param  j la coordonnée verticale de la case à récupérer
	 * @return   la case
	 */
	public Case getGrilleCase(int i, int j) {
		return this.grille[i][j];
	}
	/**
	 * Accesseur qui permet de récupérer la grille de jeu
	 * @return   la grille
	 */
	public Case[][] getGrille() {
		return this.grille;
	}
}
