package jeu;

public class Coordonnee {

	private int x;
	private int y;

	/**
	 * Constructeur
	 * @param  abscisse la coordonnée horizontale
	 * @param  ordonnee la coordonnée verticale
	 */
	public Coordonnee(int abscisse, int ordonnee) {
		this.x = abscisse;
		this.y = ordonnee;
	}

	/**
	 * Constructeur
	 */
	public Coordonnee() {
		this(0,0);
	}

	/**
	 * Méthode qui permet de modifier la coordonnée horizontale
	 * @param abscisse la coordonnée horizontale
	 */
	public void setX(int abscisse) {
		this.x = abscisse;
	}

	/**
	 * Méthode qui permet de modifier la coordonnée verticale
	 * @param ordonnee la coordonnée verticale
	 */
	public void setY(int ordonnee) {
		this.y = ordonnee;
	}

	/**
	 * Méthode qui permet de récupérer la coordonnée horizontale
	 * @return la coordonnée horizontale
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Méthode qui permet de récupérer la coordonnée verticale
	 * @return la coordonnée verticale
	 */
	public int getY() {
		return this.y;
	}

	/**
	 * Redéfinition de la méthode public boolean equals(Object o)
	 * @param  o l'objet à tester l'égalité
	 * @return   true si les deux objets coordonnées sont égaux, faux sinon
	 */
	public boolean equals(Object o){
		 boolean OK = true;

		 if(o instanceof Coordonnee){
		   OK = (this.getX() == ((Coordonnee) (o)).getX()) && (this.getY() == ((Coordonnee) (o)).getY());
		  } else {
		   OK = false;
		  }

		return OK;
	}

	public String toString(){
		return "x : " + x + " -- y : " + y;
	}
}
