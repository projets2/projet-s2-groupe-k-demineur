package jeu;

public class CaseMinee extends Case {

	/**
	 * Constructeur
	 */
	public CaseMinee() {
		super();
	}

	/**
	 * Méthode qui permet de découvrir la case
	 * @return true (case minée)
	 */
	public boolean decouvrir() {
		decouverte = true;

		return true;
	}

	/**
	 * Redéfinition de la méthode public String toString()
	 * @return une description de l'objet
	 */
	public String toString() {
		return "X";
	}

}