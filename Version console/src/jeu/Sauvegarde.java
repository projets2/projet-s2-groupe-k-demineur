package jeu;

import java.io.*;

public class Sauvegarde {

	/**
	 * Méthode permettant de sauvegarder un objet Partie
	 * @param p l'objet Partie à sauvegarder
	 */
	public static void save(Partie p) {
		File fichier =  new File("sauvegarde/partie.ser");
		try {
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichier));
			oos.writeObject(p);
		}catch (IOException e) {
			System.out.println("Une erreur est survenue :" + e.toString());
		}
	}

	/**
	 * Méthode permettant de lire et de générer un objet Partie à partir d'un fichier de sauvegarde
	 * @return l'objet Partie
	 */
	public static Partie read() {
		File fichier =  new File("sauvegarde/partie.ser");
		try {
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichier));
			return (Partie)ois.readObject();
		}catch (IOException e) {
			System.out.println("Une erreur est survenue :" + e.toString());
			return null;
		}catch (ClassNotFoundException e) {
			System.out.println("Une erreur est survenue :" + e.toString());
			return null;
		}
	}
}