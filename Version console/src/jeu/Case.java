package jeu;

import java.io.*;

@SuppressWarnings("serial")
public abstract class Case implements Serializable {
	protected boolean decouverte;
	protected Marquage marquage;
	private static int nbCaseGenerees = 1;
	protected int nuCase;
	private static int nbrDrapeau = 0;

	/**
	 * Constructeur
	 */
	public Case() {
		marquage = Marquage.NonMarque;
		nuCase = nbCaseGenerees;
		nbCaseGenerees++;
		decouverte = false;
	}

	/**
	 * Remet les variables static à leur valeur initiale
	 */
	public static void init() {
		nbCaseGenerees = 1;
		nbrDrapeau = 0;
	}

	public abstract boolean decouvrir();

	/**
	 * Méthode qui permet de marquer la case
	 */
	public void marquer() {
		if (marquage == Marquage.NonMarque) {
			marquage = Marquage.Drapeau;
			nbrDrapeau++;
		}else if (marquage == Marquage.Drapeau) {
			marquage = Marquage.Interrogation;
			nbrDrapeau--;
		}else {
			marquage = Marquage.NonMarque;
		}
	}

	/**
	 * Méthode qui permet de savoir le nombre de cases ayant un drapeau
	 * @return le nombre de drapeau
	 */
	public static int getNbDrapeau(){
		return nbrDrapeau;
	}

	/**
	 * Méthode qui permet de savoir si une case est découverte ou non
	 * @return true si la case est découverte, false sinon
	 */
	public boolean getDecouverte() {
		return decouverte;
	}
}