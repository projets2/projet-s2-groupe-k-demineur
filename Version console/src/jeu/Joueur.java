package jeu;

@SuppressWarnings("serial")
public class Joueur implements java.io.Serializable {

	private String pseudo;
	private int score;
	private long temps;

	/**
	 * Constructeur
	 */
	public Joueur() {
		this.pseudo = "";
		this.score = 0;
		this.temps = 0;
	}

	/**
	 * Constructeur de test
	 */
	public Joueur(String nom, int score, long temps) {
		this.pseudo = nom;
		this.score = score;
		this.temps = temps;
	}

	/**
	 * Méthode qui permet d'accéder au pseudo du joueur
	 * @return le pseudo du joueur
	 */
	public String getPseudo() {
		return this.pseudo;
	}

	/**
	 * Méthode qui permet de modifier le pseudo du joueur
	 * @param le pseudo du joueur
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * Méthode qui permet d'accéder au score du joueur
	 * @return le score du joueur
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * Méthode qui permet de modifier le score du joueur
	 * @param score le nouveau score
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Méthode qui permet d'accéder au temps du joueur (temps qu'il a mit pour finir la partie)
	 * @return le temps du joueur
	 */
	public long getTemps() {
		return this.temps;
	}

	/**
	 * Méthode qui permet de modifier le temps du joueur
	 * @param temps le nouveau temps
	 */
	public void setTemps(long temps) {
		this.temps = temps;
	}
}