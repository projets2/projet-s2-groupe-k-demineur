package jeu;

import java.util.*;
import java.io.*;

public class Classement {

	/**
	 * Test si le fichier de classement existe
	 * @return true si il existe, false sinon
	 */
	public static boolean exist() {
		boolean exist = false;
		File f = new File("classement.txt");
		if(f.exists() && !f.isDirectory()) {
			exist = true;
		}

		return exist;
	}

	/**
	 * Créé un fichier de classement vide
	 */
	public static void init() {
		save("");
	}

	/**
	 * Ajoute un joueur au fichier de classement
	 * @param joueur le joueur à ajouter
	 */
	public static void add(Joueur joueur) {
		if (!exist()) {
			init();
		}

		try {
			FileReader fileReader =  new FileReader("classement.txt");
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			// strToListJoueur
			ArrayList<Joueur> classement = new ArrayList<Joueur>();

			String fileStr = bufferedReader.readLine();

			if (fileStr != null) {
				String[] tabJoueur = fileStr.split("[;]+");
				for (int i = 0; i < tabJoueur.length; i++) {
					String[] tmpJoueur = tabJoueur[i].split("[,]+");
					Joueur j = new Joueur(tmpJoueur[0], Integer.parseInt(tmpJoueur[1]), Long.parseLong(tmpJoueur[2]));
					classement.add(j);
				}
			}

			classement.add(joueur);
			triRapide(classement, 0, classement.size()-1);
			Collections.reverse(classement);

			// listJoueurToStr
			String str = "";
			for (int i = 0; i < (classement.size()-1); i++) {
				Joueur j = classement.get(i);
				str += j.getPseudo()+","+j.getScore()+","+j.getTemps()+";";
			}
			Joueur j = classement.get(classement.size()-1);
			str += j.getPseudo()+","+j.getScore()+","+j.getTemps();

			save(str);

			bufferedReader.close();
		}catch(FileNotFoundException ex) {
			System.out.println("Impossible d'ouvrir le fichier de sauvegarde du classement.");
		}catch(IOException ex) {
			System.out.println("Erreur lors de l'écriture du fichier de sauvegarde du classement.");
		}
	}

	/**
	 * Méthode permettant d'échanger deux joueurs de place dans la liste. Méthode servant pour le tri rapide
	 * @param list la liste de Joueur
	 * @param a    le premier joueur à échanger
	 * @param b    le second joueur à échanger
	 */
	public static void echanger(ArrayList<Joueur> list, int a, int b) {
	    Joueur tmp = list.get(a);
	    list.remove(a);
	    list.add(a, list.get(b-1));
	    list.remove(b);
	    list.add(b, tmp);
	}

	/**
	 * Implémentation d'un tri rapide pour trier les joueurs
	 * @param list  la liste de Joueur à trier
	 * @param debut le début	
	 * @param fin   la fin
	 */
	public static void triRapide(ArrayList<Joueur> list, int debut, int fin) {
	    int gauche = debut-1;
	    int droite = fin+1;
	    int pivot = list.get(debut).getScore();
		// Si le tableau est de longueur nulle, il n'y a rien à faire
	    if(debut >= fin)
	        return;

	    while(true) {
	        do droite--; while(list.get(droite).getScore() > pivot);
	        do gauche++; while(list.get(gauche).getScore() < pivot);

	        if(gauche < droite)
	            echanger(list, gauche, droite);
	        else break;
	    }

	    triRapide(list, debut, droite);
	    triRapide(list, droite+1, fin);
	}

	/**
	 * Sauvegarde dans le fichier de sauvegarde
	 * @param str la chaine de caractères qui contient le classement
	 */
	public static void save(String str) {
		try {
			FileWriter fileWriter = new FileWriter("classement.txt");
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(str);
			bufferedWriter.close();
		}catch(IOException ex) {
			System.out.println("Erreur lors de l'écriture du fichier de sauvegarde du classement.");
		}
	}

	/**
	 * Redéfinition de la méthode toString()
	 * @return la chaine de caractères affichant le tableau de classement
	 */
	public String toString() {
		String str = "";

		try {
			FileReader fileReader =  new FileReader("classement.txt");
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			String fileStr = bufferedReader.readLine();

			str += "\n  ***  Classement  ***\n\n";

			if (fileStr != null) {
				String[] tabJoueur = fileStr.split("[;]+");
				int max = (tabJoueur.length > 10) ? 10 : tabJoueur.length;
				for (int i = 0; i < max; i++) {
					String[] tmpJoueur = tabJoueur[i].split("[,]+");
					int espace = 18-tmpJoueur[0].length();
					str += "  Pseudo : "+tmpJoueur[0];
					for (int j = 0; j < espace; j++) {
						str += " ";
					}
					str += "Score : "+tmpJoueur[1]+"\n";
				}
			}

			bufferedReader.close();
		}catch(FileNotFoundException ex) {
			System.out.println("Impossible d'ouvrir le fichier de sauvegarde du classement.");
		}catch(IOException ex) {
			System.out.println("Erreur lors de l'écriture du fichier de sauvegarde du classement.");
		}

		return str;
	}
}