package jeu;

@SuppressWarnings("serial")
public class CaseNumerotee extends Case {
	private int nbCasesVoisinesMinees = 0;
	private static int nbDeFoisDecouverte = 0;

	/**
	 * Constructeur
	 * Info : Par défaut la case est numérotée à 0
	 */
	public CaseNumerotee() {
		super();
	}

	/**
	 * Remet les variables static à leur valeur initiale
	 */
	public static void init() {
		nbDeFoisDecouverte = 0;
	}

	/**
	 * Méthode qui permet de découvrir la case
	 * @return false (case non minée)
	 */
	public boolean decouvrir() {
		// Si la case n'est pas découverte
		if (!decouverte) {
			// On incrémente le nombre de fois découverte des cases numérotées
			nbDeFoisDecouverte++;
			decouverte = true;
		}

		return false;
	}

	public static int getNbDeFoisDecouverte(){
		return nbDeFoisDecouverte;
	}

	/**
	 * Méthode qui permet d'accéder nombre de cases voisines minées de la case
	 * @return le nombre de cases voisines minées de la case
	 */
	public int getNbCasesVoisinesMinees() {
		return nbCasesVoisinesMinees;
	}

	/**
	 * Incrémente le nombre de cases voisines minées de la case
	 */
	public void addCasesVoisinesMinees() {
		nbCasesVoisinesMinees++;
	}

	/**
	 * Redéfinition de la méthode public String toString()
	 * @return une description de l'objet
	 */
	public String toString() {
		return "Case numérotée avec "+nbCasesVoisinesMinees+" cases voisines minée";
	}
}