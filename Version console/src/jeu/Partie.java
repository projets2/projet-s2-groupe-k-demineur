package jeu;

import java.util.*;
import java.io.*;

@SuppressWarnings("serial")
public class Partie implements Serializable {

	// Déclaration des variables
	private int nbMines;
	private int largeur;
	private int hauteur;
	private boolean etat;
	private boolean gagner;
	private Joueur joueur;
	private Case[][] grille;
	private Chronometre temps;
	// Liste regroupant l'ensemble des coordonnees des cases minees
	private ArrayList<Coordonnee> caseMinee;
	// Liste regroupant les coordonnees dejà controlees
	private ArrayList<Coordonnee> coordControle;

	/**
	 * Constructeur
	 * @param  joueur  le pseudo du joueur
	 * @param  hauteur la hauteur de la grille
	 * @param  largeur la largeur de la grille
	 * @param  nbMines la nombre de mine(s) de la grille
	 */
	public Partie(Joueur joueur, int hauteur, int largeur, int nbMines){
		this.joueur = joueur;
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.nbMines = nbMines;
		this.grille = new Case[this.largeur][this.hauteur];
		this.caseMinee = new ArrayList<Coordonnee>();
		this.etat = true;
		this.gagner = false;
		this.temps = new Chronometre();
		CaseNumerotee.init();
		Case.init();

		initialiserGrille();
	}

	/**
	 * Méthode qui permet d'initialiser la grille de jeu
	 */
	public void initialiserGrille(){
		for(int j = 0; j < this.hauteur; j++){
			for (int i = 0; i < this.largeur; i++) {
				this.grille[i][j] = new CaseNumerotee();
			}
		}
	}

	/**
	 * Méthode qui initialise le placement des bombes et démarre le chronomètre
	 * @param coord la première coordonnée entrée par l'utilisateur qui ne doit pas être minée
	 */
	public void initialiserBombe(Coordonnee coord) {
		// Initialisation des variables locales
		int coorX = coord.getX();
		int coorY = coord.getY();
		boolean coordonneeDejaPris = false;
		int nombreMine = 0;
		Random rand;
		int x;
		int y;

		// Tant que le nombre de mines placées et différent du nombre de mines à placer
		while(nombreMine != this.nbMines){
			// On tire des coordonnées aléatoires
			rand = new Random();
			x = rand.nextInt(this.largeur);
			y = rand.nextInt(this.hauteur);

			// On vérifie que cette coordonnées n'a pas déjà une mines
			coordonneeDejaPris = false;
			for(int i = 0; i < caseMinee.size(); i++) {
				Coordonnee c = new Coordonnee(x,y);
				if(caseMinee.get(i).equals(c)){
					coordonneeDejaPris = true;
					break;
				}
			}

			// Si la coordonnée n'est pas prise et est différente de la case de départ on rend la case minée
			if(!coordonneeDejaPris && (x != coorX || y != coorY)){
				this.grille[x][y] = new CaseMinee();
				caseMinee.add(new Coordonnee(x,y));
				nombreMine++;
			}
		}

		// Pour chaque case minée on calcule le nombre de cases minées voisines
		for (int i = 0; i < caseMinee.size(); i++) {
			Coordonnee c = caseMinee.get(i);
			x = c.getX();
			y = c.getY();

			if (((x+1) < largeur) && (grille[x+1][y] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x+1][y]).addCasesVoisinesMinees();
			}
			if (((y+1) < hauteur) && (grille[x][y+1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x][y+1]).addCasesVoisinesMinees();
			}
			if (((x-1) >= 0) && (grille[x-1][y] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x-1][y]).addCasesVoisinesMinees();
			}
			if (((y-1) >= 0) && (grille[x][y-1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x][y-1]).addCasesVoisinesMinees();
			}
			if (((x+1) < largeur) && ((y+1) < hauteur) && (grille[x+1][y+1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x+1][y+1]).addCasesVoisinesMinees();
			}
			if (((x+1) < largeur) && ((y-1) >= 0) && (grille[x+1][y-1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x+1][y-1]).addCasesVoisinesMinees();
			}
			if (((x-1) >= 0) && ((y+1) < hauteur) && (grille[x-1][y+1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x-1][y+1]).addCasesVoisinesMinees();
			}
			if (((x-1) >= 0) && ((y-1) >= 0) && (grille[x-1][y-1] instanceof CaseNumerotee)) {
				((CaseNumerotee)grille[x-1][y-1]).addCasesVoisinesMinees();
			}
		}

		// On lance le chronomètre
		this.temps.start();
	}

	/**
	 * Redéfinition de la méthode toString()
	 * @return le plateau de jeu
	 */
	public String toString() {
		this.afficherTemps();
		return toString2();
	}

	public String toString2() {

		System.out.println("\nDrapeau : " +Case.getNbDrapeau()+ "/" +this.nbMines);
		String str = "\n    ";
		String line = "    ";

		for (int i = 0; i < this.largeur; i++) {
			line += "+---";
		}
		line += "+\n";

		for (int i = 0; i < this.largeur; i++) {
			if(i < 9){
				str += " ";
			}
			str += " "+(i+1)+" ";
		}
		str += "\n";

		for(int j = 0; j < this.hauteur; j++){
			str += line+" "+(j+1)+" ";
			if(j < 9){
				str += " ";
			}
			for(int i = 0; i < this.largeur; i++){
				if (grille[i][j] != null) {
					if (grille[i][j].getDecouverte()) {
						if (grille[i][j] instanceof CaseNumerotee) {
							if (((CaseNumerotee)grille[i][j]).getNbCasesVoisinesMinees() == 0) {
								str += "|   ";
							}else {
								String osName = System.getProperty("os.name");
								int numCase = ((CaseNumerotee)grille[i][j]).getNbCasesVoisinesMinees();
								str += "| ";
								if (osName.equals("Windows 8") || osName.equals("Windows 7")){
									str += numCase+ " ";
								} else{
									if(numCase == 1){
										str += "\u001B[31m\u001B[1m" +numCase+ "\u001B[0m ";
									}
									if(numCase == 2){
										str += "\u001B[32m\u001B[1m" +numCase+ "\u001B[0m ";
									}
									if(numCase == 3){
										str += "\u001B[33m\u001B[1m" +numCase+ "\u001B[0m ";
									}
									if(numCase == 4){
										str += "\u001B[34m\u001B[1m" +numCase+ "\u001B[0m ";
									}
									if(numCase == 5){
										str += "\u001B[35m\u001B[1m" +numCase+ "\u001B[0m ";
									}
									if(numCase == 6){
										str += "\u001B[36m\u001B[1m" +numCase+ "\u001B[0m ";
									}
									if(numCase == 7){
										str += "\u001B[38;5;55m\u001B[1m" +numCase+ "\u001B[0m ";
									}
									if(numCase == 8){
										str += "\u001B[38;5;12m\u001B[1m" +numCase+ "\u001B[0m ";
									}
								}
							}
						}else if (grille[i][j] instanceof CaseMinee) {
							String osName = System.getProperty("os.name");
							if(osName.equals("Windows 8") || osName.equals("Windows 7")){
								str += "| X ";
							} else{
								str += "|\u001B[37m\u001B[1m X \u001B[0m";
							}
						}
					}else {
						String marquage = grille[i][j].marquage.getMarquage();
						str += "| "+marquage+" ";
					}
				}
				else{
					System.out.println("Case inexistante");
				}
			}
			str += "|\n";
		}

		str += line;

		return str;
	}

	/**
	 * Méthode qui permet de marquer une case
	 * @param coord les coordonnées de la case à miner
	 */
	public void marquage(Coordonnee coord) {
		if ((coord.getX() >= 0) && (coord.getX() < this.largeur) && (coord.getY() >= 0) && (coord.getY() < this.hauteur)) {
			grille[coord.getX()][coord.getY()].marquer();
		}else {
			System.out.println("La case que vous essaye de marquer n'existe pas.");
		}
	}

	/**
	 * Méthode qui permet de terminer la partie
	 */
	public void terminer() {
		etat = false;
		afficherMine();

		// On calcule de temps que le joueur a mit pour finir la partie
		this.temps.stop();
		joueur.setTemps(this.temps.getTempsEcoule() / 1000);

		// On calcule le score du joueur au final
		int tmpScore = 0;
		if (this.gagner) {
			if (joueur.getTemps() != 0) {
				tmpScore = (largeur*hauteur+(nbMines*10))/((int)joueur.getTemps());
			}else{
				tmpScore = 10;
			}
		}else {
			tmpScore = 0;
		}
		joueur.setScore(tmpScore);
	}

	/**
	 * Méthode qui permet de savoir si une partie est terminée ou non
	 * @return true si la partie est terminée, false sinon
	 */
	public boolean estTerminee() {
		return !etat;
	}

	/**
	 * Méthode qui permet de découvrir toutes les cases minées
	 */
	public void afficherMine(){
		Coordonnee coordMine;
		for (int i = 0; i < caseMinee.size(); i++) {
			coordMine = caseMinee.get(i);
			grille[coordMine.getX()][coordMine.getY()].decouvrir();
		}
	}

	/**
	 * Méthode qui permet de découvrir les cases en fonction des cases voisines et des cases minées
	 * @param coord la coordonnée de départ de la découverte
	 */
	public void decouvrir(Coordonnee coord){
		int x = coord.getX();
		int y = coord.getY();
		Case c = grille[x][y];
		coordControle.add(coord);

		// Si c'est une case minée
		if (c instanceof CaseMinee) {
			// la partie est terminée
			terminer();
		}else {
			// Sinon la case est une case numérotée
			CaseNumerotee cNum = (CaseNumerotee)c;
			// on la découvre
			cNum.decouvrir();
			if (cNum.getNbCasesVoisinesMinees() == 0) {
				// Création d'une variable Coordonnée temporaire
				Coordonnee tmp = new Coordonnee(x+1,y);
				// Si la coordonnée existe (pas hors des limites de la grille) et qu'elle n'a pas été déjà contrôlée
				if ((x+1) < largeur && ! coordControle.contains(tmp)) {
					// Alors on rappelle la méthode avec cette coordonnée
					decouvrir(tmp);
				}
				tmp = new Coordonnee(x,y+1);
				if ((y+1) < hauteur && ! coordControle.contains(tmp)) {
					decouvrir(tmp);
				}
				tmp = new Coordonnee(x-1,y);
				if ((x-1) >= 0 && ! coordControle.contains(tmp)) {
					decouvrir(tmp);
				}
				tmp = new Coordonnee(x,y-1);
				if ((y-1) >= 0 && ! coordControle.contains(tmp)) {
					decouvrir(tmp);
				}
				tmp = new Coordonnee(x+1,y-1);
				if (((x+1) < largeur) && ((y-1) >= 0) && ! coordControle.contains(tmp)) {
					decouvrir(tmp);
				}
				tmp = new Coordonnee(x-1,y+1);
				if (((x-1) >= 0) && ((y+1) < hauteur) && ! coordControle.contains(tmp)) {
					decouvrir(tmp);
				}
				tmp = new Coordonnee(x-1,y-1);
				if ((((x-1) >= 0) && ((y-1) >= 0)) && ! coordControle.contains(tmp)) {
					decouvrir(tmp);
				}
				tmp = new Coordonnee(x+1,y+1);
				if (((x+1) < largeur) && ((y+1) < hauteur) && ! coordControle.contains(tmp)) {
					decouvrir(tmp);
				}
			}
		}
	}

	/**
	 * Méthode permettant d'initialiser la liste des coordonnées contrôlées
	 */
	public void initCoordControle(){
		coordControle = new ArrayList<Coordonnee>();
	}

	/**
	 * Méthode qui permet de tester si le joueur a gagné la partie ou non
	 * @return true si il a gagné, false sinon
	 */
	public boolean aGagne(){
		int tmp = CaseNumerotee.getNbDeFoisDecouverte();
		if(tmp == (this.hauteur*this.largeur)-this.nbMines){
			this.gagner = true;
			terminer();
		}
		return this.gagner;
	}

	/**
	 * Méthode qui permet d'afficher le temps
	 */
	public void afficherTemps(){
		long tmpTemps = this.temps.getTempsInstantanee() / 1000;
		long minute = tmpTemps / 60;
		long seconde = tmpTemps % 60;

		System.out.println("Le temps écoulé est de : " + minute + " minute(s) et " + seconde + " seconde(s)");
	}

	/**
	 * Accesseur qui permet de récupérer la largeur
	 * @return la largeur
	 */
	public int getLargeur() {
		return this.largeur;
	}
	/**
	 * Accesseur qui permet de récupérer la hauteur
	 * @return la hauteur
	 */
	public int getHauteur() {
		return this.hauteur;
	}
	/**
	 * Accesseur qui permet de récupérer le nombre de mines
	 * @return le nombre de mines
	 */
	public int getNbMines() {
		return this.nbMines;
	}
	/**
	 * Accesseur qui permet de récupérer le joueur
	 * @return le joueur
	 */
	public Joueur getJoueur() {
		return this.joueur;
	}
	/**
	 * Accesseur qui permet de récupérer le chronomètre
	 * @return le chronomètre
	 */
	public Chronometre getChronometre() {
		return this.temps;
	}
	/**
	 * Accesseur qui permet de récupérer la liste des coordonnées des cases minées
	 * @return la liste des coordonnées des cases minées
	 */
	public ArrayList<Coordonnee> getCaseMinee() {
		return this.caseMinee;
	}
	/**
	 * Accesseur qui permet de récupérer la liste des coordonnées déjà contrôlées
	 * @return la liste des coordonnées déjà contrôlées
	 */
	public ArrayList<Coordonnee> getCoordControle() {
		return this.coordControle;
	}
	/**
	 * Accesseur qui permet de récupérer une case de la grille
	 * @param  i la coordonnée horizontale de la case à récupérer
	 * @param  j la coordonnée verticale de la case à récupérer
	 * @return   la case
	 */
	public Case getGrilleCase(int i, int j) {
		return this.grille[i][j];
	}
}



