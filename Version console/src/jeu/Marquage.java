package jeu;

public enum Marquage {

	NonMarque ("."),
	Drapeau ("P"),
	Interrogation ("?");
	
	private String marquageCase;
	
	private Marquage(String marquage) {
		this.marquageCase = marquage;
	}

	/**
	 * Accesseur qui permet de connaître la valeur d'un élément de l'énumération
	 * @return la valeur d'un enum
	 */
	public String getMarquage() {
		return this.marquageCase;
	}
}