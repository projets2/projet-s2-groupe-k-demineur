import java.util.*;
import java.io.*;
import jeu.*;

class Demineur {

	public static void main(String[] args){
		boolean recommencerUnePartie = true;

		while(recommencerUnePartie) {
			// Déclaration des variables
			Partie partie;
			boolean partieGagnee = false;
			Joueur joueur = new Joueur();
			Coordonnee caseChoisie = new Coordonnee();
			int nbDeTour = 0;
			Scanner sc = new Scanner(System.in);
			boolean choixCorrect = true;

			String choixUser;
			int choixMode;
			char recommencerPartie;

			System.out.println("\n***********************************\n");
			System.out.println("********* Jeu du démineur *********\n");
			System.out.println("***********************************\n\n");

			// On vérifie si il y a une partie sauvegardée en cours
			partie = Sauvegarde.read();
			char chargerPartie = 'n';

			if (!partie.estTerminee()) {

				System.out.print(" Une partie sauvegardée non terminée a été trouvée.\n");

				do{
					System.out.print(" Voulez-vous la charger ? (o, n, défaut n) ");
					choixUser = sc.nextLine();
					if (choixUser.length() != 0) {
						chargerPartie = choixUser.charAt(0);
					}
					choixCorrect = chargerPartie == 'o' || chargerPartie == 'n' || chargerPartie == 0;
				}while (!choixCorrect);
			}
			if (partie.estTerminee() || chargerPartie == 'n'){// Sinon on créé la partie avec les modes de jeu

				System.out.println("  1.Taille grille par défaut (16x16 et 40 mines) \n");
				System.out.println("  2.Taille grille personnalisée \n");

				int largeur;
				int hauteur;
				int nbMines;

				// Choix du mode de jeu
				do {
					System.out.print("  Mode de jeu : ");
					choixMode = sc.nextInt();
				}while (choixMode != 1 && choixMode != 2);

				if (choixMode == 1) {
					largeur = 16;
					hauteur = 16;
					nbMines = 20;
				}else {
					System.out.println("\nTaille personnalisée\n");
					do{
						System.out.print("Horizontale : ");
						largeur = sc.nextInt();

						System.out.print("Verticale : ");
						hauteur = sc.nextInt();
						if(largeur < 4 || hauteur < 4){
							System.out.println("\nLa taille de grille ne peut pas être inférieur à 5x5 \n");
						}
					}while(largeur < 4 || hauteur < 4);
					do{
						System.out.print("Nombre de mines : ");
						nbMines = sc.nextInt();
						if(nbMines > (largeur*hauteur)-10){
							System.out.println("\nLe nombre de mine doit être inférieur ou égal à " +((largeur*hauteur)-10)+ "\n");
						} else if(nbMines <= 0){
							System.out.println("\nLe nombre de mine doit être supérieur à 0");
						}
					}while(nbMines > (largeur*hauteur)-10 || nbMines <= 0);
				}

				// On créé la partir et on initialise la grille
				partie = new Partie(joueur, largeur, hauteur, nbMines);
			}

			// Affichage de la grille
			System.out.println(partie.toString2());

			// On vide le scanner
			sc.nextLine();

			// Boucle principale
			while (!(partie.estTerminee() || partieGagnee)) {
				choixCorrect = true;

				// Demande du marquage d'une case
				char marquerCase;
				do{
					marquerCase = 'n';
					System.out.println("Voulez-vous marquer une case ? (o, n, défaut n)");
					choixUser = sc.nextLine();
					if (choixUser.length() != 0) {
						marquerCase = choixUser.charAt(0);
					}
					choixCorrect = marquerCase == 'o' || marquerCase == 'n' || marquerCase == 0;
					if (!choixCorrect) {
						System.out.println("Votre choix est incorrect, veuillez recommencer.");
					}
				}while (!choixCorrect);

				// Si on choisit de marquer une case
				if (marquerCase == 'o') {
					System.out.println("Veuillez saisir les coordonnées de la case à marquer");
					// On demande au joueur quelle case il veut découvrir
					caseChoisie = askUserCoord(partie.getLargeur(), partie.getHauteur());

					// On marque la case
					partie.marquage(caseChoisie);
				}else {
				// Sinon on demande la saisie de coordonnées
					System.out.println("Veuillez saisir les coordonnées de la case à découvrir");
					// On demande au joueur quelle case veut-il découvrir
					caseChoisie = askUserCoord(partie.getLargeur(), partie.getHauteur());

					// Si c'est le premier tour, on place les mines en fonction de la case choisie (pour éviter de tomber directement sur une case minée)
					if(nbDeTour == 0){
						partie.initialiserBombe(caseChoisie);
						nbDeTour++;
					}

					// On initialise la variable de coordonnées contrôlées
					partie.initCoordControle();
					// On découvre la case choisie et les cases qui en découlent...
					partie.decouvrir(caseChoisie);
				}

				// On vérifie si le joueur a gagné
				partieGagnee = partie.aGagne();

				// Affichage de la grille
				System.out.println(partie);

				// On sauvegarde la partie en cours
				Sauvegarde.save(partie);
			}

			// Partie terminée
			System.out.println("\n\n  Partie terminée !\n");
			if (partieGagnee) {
				System.out.println("\n  Vous avez gagné la partie !");
			}else {
				System.out.println("\n  Vous avez perdu la partie !");
			}

			// On affiche le temps que le joueur a mit pour finir la partie et son score
			long tmpTemps = joueur.getTemps();
			long minute = tmpTemps / 60;
			long seconde = tmpTemps % 60;
			System.out.println("  Votre temps est de : " + minute + " minute(s) et " + seconde + " seconde(s)");
			System.out.println("  Score final : "+joueur.getScore()+"\n");

			if (partieGagnee) {
				String pseudo = "";
				do {
					System.out.print("\n  Votre pseudo : ");
					pseudo = sc.nextLine();
				}while (pseudo.length() == 0 || pseudo.length() > 15);
				joueur.setPseudo(pseudo);
				Classement.add(joueur);
			}

			System.out.print("\nAfficher le classement ? (o, n, défaut n) ");
			choixUser = sc.nextLine();
			if (choixUser.length() != 0) {
				if (choixUser.charAt(0) == 'o') {
					Classement cl = new Classement();
					System.out.println(cl);
				}
			}

			// Demande si l'utilisateur veut recommencer une nouvelle partie
			String choixRecommencerPartie;
			do{
				recommencerPartie = 'n';
				System.out.println("\nSouhaitez vous recommencer une partie ? (o, n, défaut n)");
				choixRecommencerPartie = sc.nextLine();

				if (choixRecommencerPartie.length() != 0) {
					recommencerPartie = choixRecommencerPartie.charAt(0);
				}
				choixCorrect = recommencerPartie == 'o' || recommencerPartie == 'n' || recommencerPartie == 0;
				if (!choixCorrect) {
					System.out.println("Votre choix est incorrect, veuillez recommencer.");
				}
			}while (!choixCorrect);

			if (recommencerPartie != 'o') {
				recommencerUnePartie = false;
			}
		}
	}

	/**
	 * Méthode permettant de demander à l'utilisateur des coordonnées. La méthode vérifie la conformité de ces coordonnées.
	 * @param  largeur la largeur du plateau de jeu
	 * @param  hauteur la hauteur du plateau de jeu
	 * @return         un objet Coordonnee
	 */
	private static Coordonnee askUserCoord(int largeur, int hauteur){
		Coordonnee caseChoisie = new Coordonnee();
		Scanner sc = new Scanner(System.in);
		boolean choixNonCorrect;
		boolean exceptionX;
		boolean exceptionY;
		int choixX = -1;
		int choixY = -1;

		do {
			// Demande de la coordonnée en abscisse
			do {
				System.out.print("X ? ");
				exceptionX = false;
				try {
					choixX = sc.nextInt();
				}catch (InputMismatchException e) {
					// Exception : la donnée saisie n'est pas un entier
					System.out.println("Le choix doit être un entier !");
					sc.nextLine();
					exceptionX = true;
				}
			}while (exceptionX);

			// Demande de la coordonnée en ordonnée
			do {
				System.out.print("Y ? ");
				exceptionY = false;
				try {
					choixY = sc.nextInt();
				}catch (InputMismatchException e) {
					// Exception : la donnée saisie n'est pas un entier
					System.out.println("Le choix doit être un entier !");
					sc.nextLine();
					exceptionY = true;
				}
			}while (exceptionY);

			choixNonCorrect = false;

			// Vérifie que les coordonnées ne sont pas hors de la grille
			if (choixX < 1 || choixX > largeur) {
				System.out.print("Choix incorrect pour X, ");
				choixNonCorrect = true;
			}
			if (choixY < 1 || choixY > largeur) {
				System.out.print("Choix incorrect pour Y, ");
				choixNonCorrect = true;
			}
			if (choixNonCorrect) {
				System.out.println("veuillez ressaisir de nouvelles valeurs !");
			}

		}while (choixNonCorrect);

		caseChoisie.setX(choixX-1);
		caseChoisie.setY(choixY-1);

		return caseChoisie;
	}
}
